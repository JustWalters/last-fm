# last-fm

### Simple, robust LastFM API client (for public data)

## Install

```
npm install bitbucket:JustWalters/last-fm
```

## Why this package?

The most useful data on LastFM is the public music data. When building an app that
incorporates music data from LastFM, lots of functionality provided by the LastFM
API isn't necessary – authorizing as a user, liking tracks, adding/removing tags,
getting a list of songs in the user's "library", etc.

This package only provides the LastFM API methods that use GET requests to fetch
data, making it **smaller and simpler** than the other LastFM libraries.

If this matches your use case, consider using [the package this one is forked from](https://www.npmjs.com/package/last-fm).

This package implements some additional public methods (notably user methods), and may have further additions in the future.

## Features

  - Powered by the [LastFM API](http://www.last.fm/api)
  - Lightweight library, only provides the GET methods from the Last.fm API

## Usage

First, [get an API key](https://www.last.fm/api/account/create) from Last.fm.

```js
const LastFM = require('last-fm')
const lastfm = new LastFM('API_KEY', { userAgent: 'MyApp/1.0.0 (http://example.com)' })

lastfm.trackSearch({ track: 'the greatest' }, (err, data) => {
  if (err) console.error(err)
  else console.log(data)
})
```

## API

### `lastfm = new LastFM(key, [opts])`

Create a new LastFM API client with the given public API `key`.

Since all the LastFM calls supported by this module access public data, the secret
key is not required.

If `opts` is provided, it can contain the following options:

- `opts.userAgent` - String to use as the `User-Agent` header in http requests
- `opts.minArtistListeners` - Exclude artist results with fewer than this number of "listeners" (default: `0`)
- `opts.minTrackListeners` - Exclude track results with fewer than this number of "listeners" (default: `0`)

Note: Unfortunately, there is no `opts.minAlbumListeners` since the Last.fm API does not
include listener numbers in album results (even though the data exists when you get an
individual album via `lastfm.albumInfo`)

## Convenience API

These APIs are not part of the LastFM documentation, but they use data from the API
and process it into a more useful form.

### `lastfm.search(opts, (err, data) => {})`

Search for artists, tracks, or albums by name. ([album.search](http://www.last.fm/api/show/album.search), [artist.search](http://www.last.fm/api/show/artist.search), [track.search](http://www.last.fm/api/show/track.search))

This returns the "top result" across all result types, prioritizing an exact query
match, if one exists. Otherwise, the most popular result by number of "listeners"
is used.

- `opts.q` - the search query
- `opts.limit` - the number of each type of result to fetch

## Album API

### `lastfm.albumInfo(opts, (err, data) => {})`

Get the metadata and tracklist for an album on Last.fm using the album name. ([album.getInfo](http://www.last.fm/api/show/album.getInfo))

### `lastfm.albumTopTags(opts, (err, data) => {})`

Get the top tags for an album on Last.fm, ordered by popularity. ([album.getTopTags](http://www.last.fm/api/show/album.getTopTags))

### `lastfm.albumSearch(opts, (err, data) => {})`

Search for an album by name. Returns album matches sorted by relevance. ([album.search](http://www.last.fm/api/show/album.search))

## Artist API

### `lastfm.artistCorrection(opts, (err, data) => {})`

Use the last.fm corrections data to check whether the supplied artist has a correction to a canonical artist. ([artist.getCorrection](http://www.last.fm/api/show/artist.getCorrection))

### `lastfm.artistInfo(opts, (err, data) => {})`

Get the metadata for an artist. Includes biography, truncated at 300 characters. ([artist.getInfo](http://www.last.fm/api/show/artist.getInfo))

### `lastfm.artistSimilar(opts, (err, data) => {})`

Get all the artists similar to this artist ([artist.getSimilar](http://www.last.fm/api/show/artist.getSimilar))

### `lastfm.artistTopAlbums(opts, (err, data) => {})`

Get the top albums for an artist on Last.fm, ordered by popularity. ([artist.getTopAlbums](http://www.last.fm/api/show/artist.getTopAlbums))

### `lastfm.artistTopTags(opts, (err, data) => {})`

Get the top tags for an artist on Last.fm, ordered by popularity. ([artist.getTopTags](http://www.last.fm/api/show/artist.getTopTags))

### `lastfm.artistTopTracks(opts, (err, data) => {})`

Get the top tracks by an artist on Last.fm, ordered by popularity. ([artist.getTopTracks](http://www.last.fm/api/show/artist.getTopTracks))

### `lastfm.artistSearch(opts, (err, data) => {})`

Search for an artist by name. Returns artist matches sorted by relevance. ([artist.search](http://www.last.fm/api/show/artist.search))

## Chart API

### `lastfm.chartTopArtists(opts, (err, data) => {})`

Get the top artists chart. ([chart.getTopArtists](http://www.last.fm/api/show/chart.getTopArtists))

### `lastfm.chartTopTags(opts, (err, data) => {})`

Get the top tags chart. ([chart.getTopTags](http://www.last.fm/api/show/chart.getTopTags))

### `lastfm.chartTopTracks(opts, (err, data) => {})`

Get the top tracks chart. ([chart.getTopTracks](http://www.last.fm/api/show/chart.getTopTracks))

## Geo API

### `lastfm.geoTopArtists(opts, (err, data) => {})`

Get the most popular artists on Last.fm by country. ([geo.getTopArtists](http://www.last.fm/api/show/geo.getTopArtists))

### `lastfm.geoTopTracks(opts, (err, data) => {})`

Get the most popular tracks on Last.fm last week by country. ([geo.getTopTracks](http://www.last.fm/api/show/geo.getTopTracks))

## Library API

### `lastfm.libraryArtists(opts, (err, data) => {})`

A paginated list of all the artists in a user's library, with play counts and tag counts. ([library.getArtists](http://www.last.fm/api/show/library.getArtists))

## Tag API

### `lastfm.tagInfo(opts, (err, data) => {})`

Get the metadata for a tag. ([tag.getInfo](http://www.last.fm/api/show/tag.getInfo))

### `lastfm.tagSimilar(opts, (err, data) => {})`

Search for tags similar to this one. Returns tags ranked by similarity, based on listening data. ([tag.getSimilar](http://www.last.fm/api/show/tag.getSimilar))

### `lastfm.tagTopAlbums(opts, (err, data) => {})`

Get the top albums tagged by this tag, ordered by tag count. ([tag.getTopAlbums](http://www.last.fm/api/show/tag.getTopAlbums))

### `lastfm.tagTopArtists(opts, (err, data) => {})`

Get the top artists tagged by this tag, ordered by tag count. ([tag.getTopArtists](http://www.last.fm/api/show/tag.getTopArtists))

### `lastfm.tagTopTags(opts, (err, data) => {})`

Fetches the top global tags on Last.fm, sorted by popularity (number of times used). ([tag.getTopTags](http://www.last.fm/api/show/tag.getTopTags))

### `lastfm.tagTopTracks(opts, (err, data) => {})`

Get the top tracks tagged by this tag, ordered by tag count. ([tag.getTopTracks](http://www.last.fm/api/show/tag.getTopTracks))

## Track API

### `lastfm.trackCorrection(opts, (err, data) => {})`

Use the last.fm corrections data to check whether the supplied track has a correction to a canonical track. ([track.getCorrection](http://www.last.fm/api/show/track.getCorrection))

### `lastfm.trackInfo(opts, (err, data) => {})`

Get the metadata for a track on Last.fm using the artist/track name. ([track.getInfo](http://www.last.fm/api/show/track.getInfo))

### `lastfm.trackSimilar(opts, (err, data) => {})`

Get the similar tracks for this track on Last.fm, based on listening data. ([track.getSimilar](http://www.last.fm/api/show/track.getSimilar))

### `lastfm.trackTopTags(opts, (err, data) => {})`

Get the top tags for this track on Last.fm, ordered by tag count. Supply a track & artist name. ([track.getTopTags](http://www.last.fm/api/show/track.getTopTags))

### `lastfm.trackSearch(opts, (err, data) => {})`

Search for a track by track name. Returns track matches sorted by relevance. ([track.search](http://www.last.fm/api/show/track.search))

## User API

### `lastfm.userArtistTracks(opts, (err, data) => {})`

Get a list of tracks by a given artist scrobbled by this user, including scrobble time. Can be limited to specific timeranges, defaults to all time. ([user.getArtistTracks](https://www.last.fm/api/show/user.getArtistTracks))

### `lastfm.userFriends(opts, (err, data) => {})`

Get a list of the user's friends on Last.fm. ([user.getFriends](https://www.last.fm/api/show/user.getFriends))

### `lastfm.userInfo(opts, (err, data) => {})`

Get information about a user profile. ([user.getInfo](https://www.last.fm/api/show/user.getInfo))

### `lastfm.lovedTracks(opts, (err, data) => {})`

Get the last 50 tracks loved by a user. ([user.getLovedTracks](https://www.last.fm/api/show/user.getLovedTracks))

### `lastfm.personalTags(opts, (err, data) => {})`

Get the user's personal tags. ([user.getPersonalTags](https://www.last.fm/api/show/user.getPersonalTags))

### `lastfm.recentTracks(opts, (err, data) => {})`

Get a list of the recent tracks listened to by this user. Also includes the currently playing track with the nowplaying="true" attribute if the user is currently listening. ([user.getRecentTracks](https://www.last.fm/api/show/user.getRecentTracks))

### `lastfm.topAlbums(opts, (err, data) => {})`

Get the top albums listened to by a user. You can stipulate a time period. Sends the overall chart by default. ([user.getTopAlbums](https://www.last.fm/api/show/user.getTopAlbums))

### `lastfm.topArtists(opts, (err, data) => {})`

Get the top artists listened to by a user. You can stipulate a time period. Sends the overall chart by default. ([user.getTopArtists](https://www.last.fm/api/show/user.getTopArtists))

### `lastfm.topTags(opts, (err, data) => {})`

Get the top tags used by this user. ([user.getTopTags](https://www.last.fm/api/show/user.getTopTags))

### `lastfm.topTracks(opts, (err, data) => {})`

Get the top tracks listened to by a user. You can stipulate a time period. Sends the overall chart by default. ([user.getTopTracks](https://www.last.fm/api/show/user.getTopTracks))

### `lastfm.weeklyAlbumChart(opts, (err, data) => {})`

Get an album chart for a user profile, for a given date range. If no date range is supplied, it will return the most recent album chart for this user. ([user.getWeeklyAlbumChart](https://www.last.fm/api/show/user.getWeeklyAlbumChart))

### `lastfm.weeklyArtistChart(opts, (err, data) => {})`

Get an artist chart for a user profile, for a given date range. If no date range is supplied, it will return the most recent artist chart for this user. ([user.getWeeklyArtistChart](https://www.last.fm/api/show/user.getWeeklyArtistChart))

### `lastfm.weeklyChartList(opts, (err, data) => {})`

Get a list of available charts for this user, expressed as date ranges which can be sent to the chart services. ([user.getWeeklyChartList](https://www.last.fm/api/show/user.getWeeklyChartList))

### `lastfm.weeklyTrackChart(opts, (err, data) => {})`

Get a track chart for a user profile, for a given date range. If no date range is supplied, it will return the most recent track chart for this user. ([user.getWeeklyTrackChart](https://www.last.fm/api/show/user.getWeeklyTrackChart))

## License

MIT. Copyright (c) [Feross Aboukhadijeh](http://feross.org).
